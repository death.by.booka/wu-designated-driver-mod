A mod for Wurm Unlimited that automatically tries to steer the vehicle to the nearest 45° angle.
Inspired by Wurm Online's autosteer and the Steer2Camera mod.

Requires Ago's Client Modloader.

Works automatically when riding a vehicle and using autorun.
Toggle off with dd disable or dd off in the console.
Toggle on with dd enable or dd on.
