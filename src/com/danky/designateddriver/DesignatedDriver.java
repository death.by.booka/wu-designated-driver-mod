package com.danky.designateddriver;

import com.wurmonline.client.console.ActionClass;
import javassist.*;
import org.gotti.wurmunlimited.modsupport.console.ConsoleListener;
import org.gotti.wurmunlimited.modsupport.console.ModConsole;
import org.gotti.wurmunlimited.modloader.classhooks.HookException;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.interfaces.*;

import java.util.Set;


public class DesignatedDriver implements WurmClientMod, PreInitable, ConsoleListener {
    private static boolean adjusting = false;
    private static boolean enabled = true;
    private static int tickTimer = 0;

    @Override
    public void preInit() {
        try {
            final ClassPool classPool = HookManager.getInstance().getClassPool();
            final CtClass ctPlayer = classPool.get("com.wurmonline.client.game.PlayerObj");
            final CtMethod method = ctPlayer.getDeclaredMethod("gametick");

            method.insertBefore("{ if ($0.carrierCreature != null) {com.danky.designateddriver.DesignatedDriver.adjustHeading($0.keys, $0.autoRun, $0.carrierCreature.getRot());}}");
        }
        catch (NotFoundException | CannotCompileException e) {
            throw new HookException(e);
        }

        ModConsole.addConsoleListener(this);
    }

    public static Set<ActionClass> adjustHeading (Set<ActionClass> keys, boolean autorun, float carrierRot) {
        if (autorun && enabled) {
            if (tickTimer <= 15)
                tickTimer++;

            if (adjusting) {
                keys.remove(ActionClass.MOVE_LEFT);
                keys.remove(ActionClass.MOVE_RIGHT);

                adjusting = false;
                return keys;
            } else if (tickTimer >= 15) {
                if (keys.contains(ActionClass.MOVE_LEFT) || keys.contains(ActionClass.MOVE_RIGHT))
                    return keys;

                float angleDiff = carrierRot % 45.0f;

                float margin = 0.7f;
                if (angleDiff <= margin || angleDiff + margin >= 45.0f)
                    return keys;

                if (angleDiff > 22.5f) {
                    keys.add(ActionClass.MOVE_LEFT);
                    adjusting = true;
                    tickTimer = 0;
                } else {
                    keys.add(ActionClass.MOVE_RIGHT);
                    adjusting = true;
                    tickTimer = 0;
                }
            }
        } else if (adjusting) {
            keys.remove(ActionClass.MOVE_LEFT);
            keys.remove(ActionClass.MOVE_RIGHT);
            adjusting = false;
        }

        return keys;
    }

    @Override
    public boolean handleInput(String s, Boolean aBoolean) {
        if (s == null) {
            return false;
        }

        if (s.equalsIgnoreCase("dd enable") || s.equalsIgnoreCase("dd on")) {
            enabled = true;
            System.out.println("Designated Driver enabled.");
            return true;
        } else if (s.equalsIgnoreCase("dd disable") || s.equalsIgnoreCase("dd off")) {
            enabled = false;
            System.out.println("Designated Driver disabled.");
            return true;
        }
        return false;
    }
}
